import { Routes, Route } from "react-router-dom"
import { BaseLayout } from "./BaseLayout";
import { Fale_conosco } from "./pagina-faleconosco/FaleConosco";
import { Login } from "./pagina-login/login";
import { Registrar } from "./pagina-registre-se/Registre-se";
import { Filmes } from "./pagina-Filmes/Filmes";
import { Section } from "./pagina-Sessao/Section";
import { Home } from "./pagina-home/Home";
import { Checkout } from "./pagina-checkout/Checkout";


export function Router() {
    return (
        <Routes>
            <Route path="/" element={<BaseLayout></BaseLayout>}>
                <Route path="/Login" element={<Login/>}/>               
                <Route path="/FaleConosco" element={<Fale_conosco/>}/>
                <Route path="/Registre-se" element={<Registrar/>}/>
                <Route path="/Filmes" element={<Filmes/>}/>
                <Route path="/Sessão" element={<Section/>}/>
                <Route path="/" element={<Home />}/>
                <Route path="/Checkout"  element={<Checkout />}/>
            </Route>
        </Routes>
    )
}