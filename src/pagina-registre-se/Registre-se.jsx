import styles from "./registre-se.module.css"

export function Registrar() {
    return (
       <div className={styles.fundo}>
            <div className={styles.apresentacao}>
                <h1>Junte-se à Comunidade Cinematográfica!<br></br>Cadastre-se Aqui!</h1>

                <p>Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno. <br></br> Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.</p>
            </div>
            <div className={styles.box}>
                <h1>Registre-se</h1>
                <form>
                    <input type="text" placeholder="Nome" required></input>
                    <input type="text" placeholder="Sobrenome" required></input>
                    <input type="text" placeholder="CPF" required></input>
                    <input type="text" placeholder="Data de nascimento (dd/mm/aaaa)" required></input>
                    <input type="text" placeholder="Nome de Usuário" required></input>
                    <input type="email" placeholder="E-mail" required></input>
                    <input type="password" placeholder="Senha" required></input>
                    <input type="password" placeholder="Confirmar senha" required></input>
                    <button type="submit">REGISTRAR</button>
                </form>
                

            </div>

       </div>
    )
}