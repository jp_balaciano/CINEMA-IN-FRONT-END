
import { useState } from "react"
import styles from "./login.module.css"
import { NavLink } from "react-router-dom"


export function Login() {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  const url = "http://localhost:3000/usuarios/login";


  function handleClick(e) {
    e.preventDefault()

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },

      body: JSON.stringify({ 
        login,
        password
      })
       
    
      
    }) .then((bruteData) => {
          bruteData.json().then((data) => {
            console.log(data);
          }).catch((error) => {
            console.log(error);
          })
        }).catch((error) => {
            console.log(error);
          }) 
  }

  return (
    <>
      <div className={styles.fundo}>
        <section className={styles.texto}>
          <img src="src/assets/logo-header.svg" alt="logo do cinema"></img>
          <p>Escolha suas sessões, reserve seus lugares e entre de cabeça em narrativas que cativam e emocionam. Este é o nosso convite para você vivenciar o cinema de maneira única. Nossa página de login é a porta de entrada para essa experiência excepcional, e estamos empolgados para compartilhar cada momento cinematográfico com você.</p>
        </section>
        <section className={styles.formulario}>
          <div className={styles.contato}>
              <h1>Login</h1>
              <p>Faça login e garanta o seu lugar na diversão</p>
          </div>
          <div className={styles.cadastro}>
              <input type="text" required placeholder="Usuário ou E-mail" onChange={(e) => setLogin(e.target.value)}></input>
              <input type="password" required placeholder="Senha" onChange={(e) => setPassword(e.target.value)}></input>
          </div>
          <div>
              <p>Esqueci minha senha</p>
          </div>
          <button className={styles.entrar} onClick={(e) => handleClick(e)}><NavLink className={styles.linkBtn} to="/">ENTRAR</NavLink></button>
          <h2></h2>
          <NavLink to="/Registre-se"><button className={styles.cadastrar} type="submit">CADASTRE-SE</button></NavLink>
        </section>
      </div>
      </>
  )
}