import styles from "./filmes.module.css"
import { NavLink } from "react-router-dom"
import { useState, useEffect } from "react";




export function Filmes() {

    const url = "http://localhost:3000/filmes"
    const [films, setFilm] = useState([]);
    const [filteredFilms, setFilteredFilms] = useState([]);


    useEffect(() => {
        fetch(url)
            .then((res) => res.json())
            .then((films) => {
                setFilm(films)
                setFilteredFilms(films)
            })
    }, []);

    function filmFilter(genero = "", classificacao = "") {
        console.log(genero, classificacao)

        if(!genero && !classificacao) {
            setFilteredFilms(films)
        } else {
            const filmsFiltered = films.filter((film) => {
                if(film.genre == genero || film.classification == classificacao) {
                    return film
                }
            })
            setFilteredFilms(filmsFiltered)
        }
    }

    return (
        <>
            <div className={styles.banner}>
                <section className={styles.pesquisa}>
                    <input type="text" placeholder="Pesquisar filmes"></input>
                    <img src="src/assets/lupa.svg"></img>
                </section>
                <section className={styles.caixinha}>
                    <select className={styles.genero}>
                    <option onClick={() => filmFilter("Ação")}>Ação</option>
                    <option onClick={() => filmFilter("Animação")}>Animação</option>
                    <option onClick={() => filmFilter("Aventura")}>Aventura</option>
                    <option onClick={() => filmFilter("Comédia")}>Comédia</option>
                    <option onClick={() => filmFilter("Drama")}>Drama</option>
                    <option onClick={() => filmFilter("Fantasia")}>Fantasia</option>
                    </select>
                    <select className={styles.classificacao}>
                        <option onClick={() => filmFilter("Livre")}>Livre</option>
                        <option onClick={() => filmFilter("10")}>10 anos</option>
                        <option onClick={() => filmFilter("12")}>12 anos</option>
                        <option onClick={() => filmFilter("14")}>14 anos</option>
                        <option onClick={() => filmFilter("16")}>16 anos</option>
                    </select> 
                </section>
            </div>
            <div className={styles.fundo}>
                <div className={styles.geral}>
                    <h1>Filmes</h1>
                    <div className={styles.line}>
                        {filteredFilms.map((film) => {

                            
                            return (
    
                                
                                // eslint-disable-next-line react/jsx-key
                                <section className={styles.card}>
                                    <img src={film.url} alt={"Capa do filme: " + film.title}></img>
                                    <div className={styles.nome}>
                                        <h1>{film.title}</h1>
                                    
                                            
                                                {
                                                    film.classification == 0?  <p className={`${styles.zero} ${styles.idade1}`}>L</p> : 
                                            
                                                    film.classification == 1? <p className={`${styles.um} ${styles.idade1}`}>10</p> : 

                                                    film.classification == 2? <p className={`${styles.dois} ${styles.idade1}`}>12</p> :

                                                    film.classification == 3? <p className={`${styles.tres} ${styles.idade1}`}>14</p> :

                                                    film.classification == 4? <p className={`${styles.quatro} ${styles.idade1}`}>16</p> :

                                                    <p className={`${styles.cinco} ${styles.idade1}`}>18</p> 
                                                }
                                            
                                    </div>
                                    <div className={styles.text1}>
                                        <p>{film.genre}</p>
                                        <p>Direção: {film.director}</p>
                                        <p>{film.sinopsys}</p>
                                        <button className={styles.botao1} type="submit"><NavLink className={styles.linkBtn} to="/Sessão">VER SESSÕES</NavLink></button>
                                    </div>
                                </section>
                            )
                        })}
                        
                    </div>
                </div>
            </div>
        </>
    )
}