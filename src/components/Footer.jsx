import styles from "./footer.module.css"


function Footer() {
    return (
        <div>
            <div className={styles.footer}>
                <section>
                    <div className={styles.endereco}>
                        <h1>Endereço</h1>
                        <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ</p>
                        <p>CEP: 24210-315</p>
                    </div>
                    <div className={styles.contato}>
                        <h2>Fale conosco</h2>
                        <p>contato@injunior.com.br</p>
                        <div>
                            <img src="src/assets/instagram.svg" alt="icone instagram"></img>
                            <img src="src/assets/facebook.svg" alt="icone facebook"></img>
                            <img src="src/assets/linkedin.svg" alt="icone linkedin"></img>
                        </div>
                    </div>
                </section> 
                <section className={styles.INgresso}>
                    <img src="src/assets/INgresso.svg" alt="ingresso do footer da IN"></img>
                </section>   
                <section className={styles.mapa}>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1885898739706!2d-43.13581588830351!3d-22.906413837772725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1692490822957!5m2!1spt-BR!2sbr" width="325" height="245" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </section>
            </div>
            <div className={styles.Copyright}>
                <p>© Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.</p>
            </div>
        </div>
    )
}

export default Footer 