import { NavLink } from "react-router-dom"
import styles from "./header.module.css"


function Header(){
    return (
        <div className={styles.header}>
            <img src="src/assets/logo-header.svg" alt="logo do cinema IN"></img>
            <div>
                <NavLink to="/Filmes"><img className={styles.image} src="src/assets/Movies-header.svg" alt="icone dos filmes"></img></NavLink>
                <NavLink to="/Login"><img className={styles.image} src="src/assets/Sign-header.svg" alt="icone do sign in"></img></NavLink>
                <NavLink to="/FaleConosco"><img className={styles.image} src="src/assets/Help-header.svg" alt="icone do help"></img></NavLink>
            </div>
        </div>
    )
}

export default Header 


