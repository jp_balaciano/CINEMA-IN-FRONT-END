import styles from "./checkout.module.css"


export function Checkout (){
    return (
        <>
        <div className={styles.fundo}>
            <aside>
                <div className={styles.parte_cima}>
                    <img src="src/assets/Filme1.svg" alt="filme besouro azul"></img>
                    <section>
                        <h2>Besouro Azul</h2>
                        <div className={styles.caracteristicas}>
                            <div className={styles.retangulo}>
                                <p>2D</p>
                            </div>
                            <div className={styles.retangulo}>
                                <p>15:20</p>
                            </div>
                        </div>
                    </section>
                </div>
                <div className={styles.parte_baixo}>
                    <section className={styles.titulo}>
                        <img src="src/assets/cadeira-checkout.svg "></img>
                        <p>ASSENTOS ESCOLHIDOS</p>
                    </section>
                    <section className={styles.box}>
                    </section>
                    <button type="submit">CONFIRMAR</button>
                </div>
            </aside>
            <div className={styles.sala}>
                <section className={styles.tela}>
                    <div>
                        <p>TELA</p>
                    </div>
                    <div></div>
                </section>
                <section className={styles.fileiras}>
                    <div className={styles.letras}>
                        <p>A</p>
                        <p>B</p>
                        <p>C</p>
                        <p>D</p>
                        <p>E</p>
                        <p>F</p>
                        <p>G</p>
                        <p>H</p>
                        <p>I</p>
                        <p>J</p>
                    </div>
                    <div className={styles.cadeiras}>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                        <div className={styles.fila}>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                            <div className={styles.assentos}></div>
                        </div>
                    </div>
                    <div className={styles.letras}>
                        <p>A</p>
                        <p>B</p>
                        <p>C</p>
                        <p>D</p>
                        <p>E</p>
                        <p>F</p>
                        <p>G</p>
                        <p>H</p>
                        <p>I</p>
                        <p>J</p>
                    </div>
                </section>
                <section className={styles.legenda}>
                    <div className={styles.linha}></div>
                    <h2>LEGENDA</h2>
                    <div className={styles.significado}>
                        <div className={styles.disponivel}>
                            <div></div>
                            <p>Disponível</p>
                        </div>
                        <div className={styles.selecionado}>
                            <div></div>
                            <p>Selecionado</p>
                        </div>
                        <div className={styles.comprado}>
                            <div></div>
                            <p>Comprado</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        </>
    )
}