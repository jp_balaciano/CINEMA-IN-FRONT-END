
import styles from "./faleconosco.module.css"

export function Fale_conosco() {
    return (
        <div className={styles.fundo}>
            <div className={styles.caixa}>
                <section className={styles.contato}>
                    <h1>Contato</h1>
                    <p>Encontrou algum problema? <br></br>Envie uma mensagem!</p>
                </section>
                <section className={styles.form}>
                    <input type="text" placeholder="Nome Completo"></input>
                    <input type="text" placeholder="Assunto"></input>
                    <textarea placeholder="Descrição Detalhada"></textarea>
                    <button type="submit">ENVIAR</button>
                </section>
            </div>
        </div>
    )
}