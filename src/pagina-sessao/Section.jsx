import styles from "./section.module.css"
import { NavLink } from "react-router-dom"

export function Section() {
    return (
        <>
        <div className={styles.fundo}>
            <div className={styles.banner}>
                <img src="src/assets/Filme1.svg" alt="filme"></img>
                <section className={styles.description}>
                    <div className={styles.titulo}>
                        <h1>Besouro Azul</h1>
                        <div className={styles.idade}>
                            <p>12</p>
                        </div>
                    </div>
                    <div className={styles.detalhes}>
                        <p>Ação, Aventura</p>
                        <p>Quando um escaravelho alienígena se funde com seu corpo, Jaime ganha uma armadura tecnológica que lhe concede superpoderes incríveis.</p>
                    </div>
                    <div className={styles.filtros}>
                        <select>
                            <option>Rio de Janeiro</option>
                            <option>São Paulo</option>
                            <option>Minas Gerais</option>
                        </select>
                        <select>
                            <option>Rio de Janeiro</option>
                            <option>São Paulo</option>
                            <option>Minas Gerais</option>
                        </select>
                    </div>
                </section>
            </div>
            <div className={styles.horarios}>
                <section className={styles.tipo}>
                    <div>
                        <p>2D</p>
                    </div>
                    <div>
                        <p>3D</p>
                    </div>
                    <div>
                        <p>IMAX</p>
                    </div>
                </section>
                <section className={styles.quadro}>
                    <div className={styles.box}>
                        <section className={styles.d}>
                            <p>2D</p>
                        </section>
                        <section className={styles.comeco}>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>15:20</p>
                            </div></NavLink>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>17:40</p>
                            </div></NavLink>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>20:00</p>
                            </div></NavLink>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>22:10</p>
                            </div></NavLink>
                        </section>
                    </div>
                    <div className={styles.box}>
                        <section className={styles.d}>
                            <p>3D</p>
                        </section>
                        <section className={styles.comeco}>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>15:30</p>
                            </div></NavLink>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>20:15</p>
                            </div></NavLink>
                        </section>
                    </div>
                    <div className={styles.box}>
                        <section className={styles.d}>
                            <p>IMAX</p>
                        </section>
                        <section className={styles.comeco}>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>16:20</p>
                            </div></NavLink>
                            <NavLink to="/Checkout"><div className={styles.caixa_hora}>
                                <p>18:00</p>
                            </div></NavLink>
                        </section>
                    </div>
                </section>
            </div>
        </div>
        </>
    )
}